import {SideBar} from "@/app/ui/sidebard/SideBar";

export default function Home() {
    //min-h-screen flex-col items-center justify-between p-24
  return (
    <main className="divide-x flex">
        <main className='flex-1'>
            <div className='flex justify-center items-center h-screen'>
                <h1 className='text-4xl font-bold'>Hello World</h1>
            </div>
        </main>
    </main>
  )
}
