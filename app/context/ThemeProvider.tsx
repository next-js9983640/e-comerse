'use client'

import {createContext, ReactNode, useState} from 'react'

export const ThemeContext = createContext({})


export function ThemeProvider({ children }:{children: ReactNode}) {
    const [theme, setTheme] = useState()

    const value = {theme, setTheme}
    return (
        <ThemeContext.Provider value={value}>
            <div className={theme}>
                {children}
            </div>
        </ThemeContext.Provider>
    )
}