import { Scrollbars } from 'react-custom-scrollbars-2'
import { ReactNode } from 'react'
export default function ScrollBar({children}:{ children?: ReactNode}){
    return(
        <Scrollbars
            autoHide
            universal
            renderThumbVertical={() => {
                return (
                    <div
                        className='hover:bg-slate-500'
                        style={{
                            width: 5,
                            background: '#000',
                            borderRadius: '10px',
                            transition: 'ease-in 3s',
                        }}
                    />
                );
            }}
        >
            {children}
        </Scrollbars>
    )
}
