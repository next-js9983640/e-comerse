import Link from "next/link";

export default function Nav(){
    return(
        <nav>
            <ul>
                <li>
                    <Link href='/home' >
                        Home
                    </Link>
                </li>
                <li>
                    <Link href='/profile'>
                        Profile
                    </Link>
                </li>
                <li>
                    <Link href='/settings'>
                        Settings
                    </Link>
                </li>
            </ul>
        </nav>
    )
}