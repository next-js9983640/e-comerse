import ScrollBar from "@/app/ui/components/ScrollBar";
import Nav from "@/app/ui/sidebard/SideBarNav";
import {Hero} from "@/app/ui/icons/Hero";

export function SideBar(){
    return(
        <>
            <div className="flex flex-col w-[15%] divide-y h-screen">
                <div className='flex flex-col justify-center items-center'>
                    <Hero width={150} height={150}/>
                </div>
                <div className='flex-1 overflow-y-auto py-4 pl-5'>
                    <Nav/>
                </div>
                <div className='flex justify-center items-center py-4 px-3 bg-blue-950'>
                    <button>Logout</button>
                </div>
            </div>
        </>
    )
}