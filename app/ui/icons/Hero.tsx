export function Hero({width=100, height=100}){
    return (
        <div className='flex relative md:flex-col flex-row justify-center items-center bg-blend-color-burn'>
            <svg
                width={width}
                height={height}
                viewBox="0 0 1300 1300"
                className='fill-current text-white m-0 p-0'
            >
                <g>
                    <g>
                        <g>
                            <linearGradient
                                id="SVGID_1_"
                                gradientUnits="userSpaceOnUse"
                                x1={657.6122}
                                y1={369.3794}
                                x2={896.9167}
                                y2={369.3794}
                            >
                                <stop
                                    offset={0.0051}
                                    style={{
                                        stopColor: "#826AAF",
                                    }}
                                />
                                <stop
                                    offset={0.2602}
                                    style={{
                                        stopColor: "#522E91",
                                    }}
                                />
                                <stop
                                    offset={1}
                                    style={{
                                        stopColor: "#782B90",
                                    }}
                                />
                            </linearGradient>
                            <polygon
                                style={{
                                    fill: "url(#SVGID_1_)",
                                }}
                                points="896.917,300.298 657.612,438.461 674.568,326.788  "
                            />
                            <linearGradient
                                id="SVGID_00000176002350419832907680000017491114146581687434_"
                                gradientUnits="userSpaceOnUse"
                                x1={657.6122}
                                y1={389.9557}
                                x2={896.9167}
                                y2={389.9557}
                            >
                                <stop
                                    offset={0}
                                    style={{
                                        stopColor: "#150958",
                                    }}
                                />
                                <stop
                                    offset={1}
                                    style={{
                                        stopColor: "#522E91",
                                    }}
                                />
                            </linearGradient>
                            <polygon
                                style={{
                                    fill: "url(#SVGID_00000176002350419832907680000017491114146581687434_)",
                                }}
                                points="896.917,300.298  762.801,479.613 657.612,438.461  "
                            />
                        </g>
                        <g>
                            <linearGradient
                                id="SVGID_00000002357613681922621220000016904942442931536058_"
                                gradientUnits="userSpaceOnUse"
                                x1={694.1169}
                                y1={727.9703}
                                x2={694.1169}
                                y2={451.6452}
                            >
                                <stop
                                    offset={0}
                                    style={{
                                        stopColor: "#782B90",
                                    }}
                                />
                                <stop
                                    offset={0.6837}
                                    style={{
                                        stopColor: "#522E91",
                                    }}
                                />
                                <stop
                                    offset={0.9949}
                                    style={{
                                        stopColor: "#826AAF",
                                    }}
                                />
                            </linearGradient>
                            <polygon
                                style={{
                                    fill: "url(#SVGID_00000002357613681922621220000016904942442931536058_)",
                                }}
                                points="650,451.645 650,727.97  738.234,522.165  "
                            />
                            <linearGradient
                                id="SVGID_00000012464320867461001560000004259856193719207097_"
                                gradientUnits="userSpaceOnUse"
                                x1={605.8832}
                                y1={727.9703}
                                x2={605.8832}
                                y2={451.6452}
                            >
                                <stop
                                    offset={0}
                                    style={{
                                        stopColor: "#150958",
                                    }}
                                />
                                <stop
                                    offset={1}
                                    style={{
                                        stopColor: "#522E91",
                                    }}
                                />
                            </linearGradient>
                            <polygon
                                style={{
                                    fill: "url(#SVGID_00000012464320867461001560000004259856193719207097_)",
                                }}
                                points="561.766,522.165 650,451.645  650,727.97  "
                            />
                        </g>
                        <g>
                            <linearGradient
                                id="SVGID_00000047022996278662920660000009620484531806442166_"
                                gradientUnits="userSpaceOnUse"
                                x1={403.0833}
                                y1={389.9558}
                                x2={642.388}
                                y2={389.9558}
                            >
                                <stop
                                    offset={0.0051}
                                    style={{
                                        stopColor: "#826AAF",
                                    }}
                                />
                                <stop
                                    offset={0.4184}
                                    style={{
                                        stopColor: "#522E91",
                                    }}
                                />
                                <stop
                                    offset={1}
                                    style={{
                                        stopColor: "#782B90",
                                    }}
                                />
                            </linearGradient>
                            <polygon
                                style={{
                                    fill: "url(#SVGID_00000047022996278662920660000009620484531806442166_)",
                                }}
                                points="642.388,438.461  403.083,300.298 537.199,479.613  "
                            />
                            <linearGradient
                                id="SVGID_00000088827861035333623340000004654757200943736452_"
                                gradientUnits="userSpaceOnUse"
                                x1={403.0833}
                                y1={369.3795}
                                x2={642.388}
                                y2={369.3795}
                            >
                                <stop
                                    offset={0}
                                    style={{
                                        stopColor: "#150958",
                                    }}
                                />
                                <stop
                                    offset={1}
                                    style={{
                                        stopColor: "#522E91",
                                    }}
                                />
                            </linearGradient>
                            <polygon
                                style={{
                                    fill: "url(#SVGID_00000088827861035333623340000004654757200943736452_)",
                                }}
                                points="625.433,326.788  642.388,438.461 403.083,300.298  "
                            />
                        </g>
                    </g>
                </g>
            </svg>
            <div className='text-center absolute'>
                <h1 className='text-2xl font-extrabold text-[#724d9d]'>Good</h1>
                <p className='text-sm text-white'>Games</p>
            </div>
        </div>
    )
}