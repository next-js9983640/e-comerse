# E-comerce 
Next.js + Tailwind CSS + TypeScript + ESLint + Prettier + Jest + React Testing Library + Cypress
This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).


First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

- web template
- [ ] [ui home](https://www.behance.net/gallery/87081313/Good-games)

![img.png](./public/90b7lrci.png)
 
- dashboard template admin

- [ ] [ui dashboard](https://www.behance.net/gallery/183125153/Crypto-Assets-Management-Dashboard-Design)

![img.png](./public/mag8bv1u.png)

![img.png](./public/uq1re99j.png)

- alternativa 2

![img.png](public/ptcnmvlc.png)

